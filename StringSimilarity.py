__author__ = 'alexf4'

import sys

def main():

    #Grab the input data
    T =  sys.stdin.readline()

    strings = []

    #Clean input
    for repeater in range (0,int(T)):
        inputdata = sys.stdin.readline()
        strings.append(inputdata.rstrip())

    #for each element in strings
    for string in strings:

        #generate succicies
        sufficies = generateSufficies(string)

        score = 0

        for suffix in sufficies:
            #compare prefix
            score = score + comparePrefix(string, suffix)

        print score


def comparePrefix(input1, input2):

    input2Preficies = generatePreficies(input2)

    for prefix in input2Preficies:

         if( input1.startswith( prefix)):
          return len(prefix)
    return 0

def generatePreficies (input1):
    retval = []

    if (len(input1)  == 1):
        databack = [input1]
        return databack
    else :
        retval.append(input1)
        temp = (generatePreficies(input1[:len(input1) -1]))

        retval.extend(temp)
    return retval

def generateSufficies(input1):
    retval = []

    if (len(input1)  == 1):
        databack = [input1]
        return databack
    else :
        retval.append(input1)
        temp = (generateSufficies(input1[1:]))

        retval.extend(temp)
    return retval



if __name__ == "__main__":
    main()

